package com.pilot.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "Asset_Details") 
public class AssetDetails {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Asset_Id",unique = true, nullable = false)
	@Id
	private int assetId;
	
	@Column(name = "Serial_Number")  
	private String serialNumber;
	
	@Column(name = "Host_Name") 
	private String hostName;
	
	@Column(name = "OS_Type")  
	private String osType;
	
	@Column(name = "Asset_Model_Id")  
	private Integer assetModelId;
	
	@Column(name = "Dual_Band")  
	private String dualBand;
	
	@Column(name = "IMEI_Number")  
	private String IMEINumber;
	
	@Column(name = "Ram_Upgrade")  
	private String ramUpgrade;
	
	@Column(name = "Ram_Serial_Number")  
	private String ramSerialNumber;
	
	@Column(name = "Ram_Part_Number")  
	private String ramPartNumber;
	
	@Column(name = "Difference_In_Days")  
	private String differenceInDays;
	
	@Column(name = "Asset_Status")  
	private String assetStatus;
	
	@Column(name = "Vendor")  
	private String vendor;
	
	@Column(name = "Fiscal_Year")  
	private String fiscalYear;
	
	@Column(name = "Complaint")  
	private String complaint;
	
	@Column(name = "Comments")  
	private String comments;

	@Type(type="json")
	@Column(name = "Additional_Attributes", columnDefinition ="json")  
	private List<Attribute> additionalAttributes;
	
	
	public int getAssetId() {
		return assetId;
	}


	public void setAssetId(int assetId) {
		this.assetId = assetId;
	}


	public String getSerialNumber() {
		return serialNumber;
	}


	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}


	public String getHostName() {
		return hostName;
	}


	public void setHostName(String hostName) {
		this.hostName = hostName;
	}


	public String getOsType() {
		return osType;
	}


	public void setOsType(String osType) {
		this.osType = osType;
	}


	public Integer getAssetModelId() {
		return assetModelId;
	}


	public void setAssetModelId(Integer assetModelId) {
		this.assetModelId = assetModelId;
	}


	public String getDualBand() {
		return dualBand;
	}


	public void setDualBand(String dualBand) {
		this.dualBand = dualBand;
	}


	public String getIMEINumber() {
		return IMEINumber;
	}


	public void setIMEINumber(String iMEINumber) {
		IMEINumber = iMEINumber;
	}


	public String getRamUpgrade() {
		return ramUpgrade;
	}


	public void setRamUpgrade(String ramUpgrade) {
		this.ramUpgrade = ramUpgrade;
	}


	public String getRamSerialNumber() {
		return ramSerialNumber;
	}


	public void setRamSerialNumber(String ramSerialNumber) {
		this.ramSerialNumber = ramSerialNumber;
	}


	public String getRamPartNumber() {
		return ramPartNumber;
	}


	public void setRamPartNumber(String ramPartNumber) {
		this.ramPartNumber = ramPartNumber;
	}


	public String getDifferenceInDays() {
		return differenceInDays;
	}


	public void setDifferenceInDays(String differenceInDays) {
		this.differenceInDays = differenceInDays;
	}


	public String getAssetStatus() {
		return assetStatus;
	}


	public void setAssetStatus(String assetStatus) {
		this.assetStatus = assetStatus;
	}


	public String getVendor() {
		return vendor;
	}


	public void setVendor(String vendor) {
		this.vendor = vendor;
	}


	public String getFiscalYear() {
		return fiscalYear;
	}


	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}


	public String getComplaint() {
		return complaint;
	}


	public void setComplaint(String complaint) {
		this.complaint = complaint;
	}


	public String getComments() {
		return comments;
	}


	public void setComments(String comments) {
		this.comments = comments;
	}



	public List<Attribute> getAdditionalAttributes() {
		return additionalAttributes;
	}


	public void setAdditionalAttributes(List<Attribute> additionalAttributes) {
		this.additionalAttributes = additionalAttributes;
	}


	/**
	 * toString() return all the variable values
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
