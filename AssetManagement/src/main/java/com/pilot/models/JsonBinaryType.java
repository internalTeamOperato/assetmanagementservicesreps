package com.pilot.models;

import java.util.Properties;

import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.usertype.DynamicParameterizedType;

import com.pilot.utilities.JsonBinarySqlTypeDescriptor;
import com.pilot.utilities.JsonTypeDescriptor;

public class JsonBinaryType
extends AbstractSingleColumnStandardBasicType<Object> 
implements DynamicParameterizedType {

public JsonBinaryType() {
    super( 
        JsonBinarySqlTypeDescriptor.INSTANCE, 
        new JsonTypeDescriptor()
    );
}

public String getName() {
    return "jsonb";
}

@Override
public void setParameterValues(Properties parameters) {
    ((JsonTypeDescriptor) getJavaTypeDescriptor())
        .setParameterValues(parameters);
}

}
