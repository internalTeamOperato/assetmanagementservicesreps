package com.pilot.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "asset_type") 
public class AssetMasterData {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ASSET_TYPE_ID",unique = true, nullable = false)
	@Id
	private int assetId;
	
	@Column(name = "ASSET_TYPE")  
	private String assetType;
	
	@Column(name = "ASSET_INTRO_DT") 
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd")
	private Date assetIntroDate;
	
	@Column(name = "COMMENTS",columnDefinition = "mediumText")  
	private String comments;
	
	@Column(name = "UPD_ID")  
	private String updateId;
	
	@Column(name = "UPD_DT")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern ="dd-MM-yyyy")
	private Date updateDate;
	
	@Column(name = "ACTV_IDC")  
	private String activeIndicator;
	
	@Column(name = "ASSET_CATEGORY")  
	private String assetCategory;

	public int getAssetId() {
		return assetId;
	}

	public void setAssetId(int assetId) {
		this.assetId = assetId;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public Date getAssetIntroDate() {
		return assetIntroDate;
	}

	public void setAssetIntroDate(Date assetIntroDate) {
		this.assetIntroDate = assetIntroDate;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getUpdateId() {
		return updateId;
	}

	public void setUpdateId(String updateId) {
		this.updateId = updateId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getActiveIndicator() {
		return activeIndicator;
	}

	public void setActiveIndicator(String activeIndicator) {
		this.activeIndicator = activeIndicator;
	}

	/**
	 * toString() return all the variable values
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getAssetCategory() {
		return assetCategory;
	}

	public void setAssetCategory(String assetCategory) {
		this.assetCategory = assetCategory;
	}

	
}
