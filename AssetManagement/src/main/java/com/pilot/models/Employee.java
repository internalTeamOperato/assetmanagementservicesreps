/**
 * 
 */
package com.pilot.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Lowes
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "Employee") 
public class Employee {

	@Column(name = "Employee_Id")
	private String employeeId;
	
	@Column(name = "NT_Id",unique=true)
	private String ntId;
	
	@Column(name = "UID")
	@Id
	private int uId;
	
	@Column(name = "Employee_Name")
	private String employeeName;
	
	@Column(name = "Department_Name")
	private String departmentName;
	
	@Column(name = "Department_Number")
	private String departmentNumber;
	
	@Column(name = "Manager_Name")
	private String managerName;
	
	@Column(name = "Email_Id")
	private String emailId;
	
	@Column(name = "Employment_Type")
	private String employmentType;
	
	@Column(name = "Date_Of_Joining")
	private Date dateOfJoining;
	
	@Column(name = "Manager_Employee_Id")
	private String managerEmployeeId;
	
	@Column(name = "Employment_Status")
	private String employmentStatus;
	
	@Column(name = "Designation")
	private String designation;
	
	@Column(name = "ID_Creation_Date")
	private Date idCreationDate;
	
	@Column(name = "Extension_Number")
	private int extensionNumber;
	
	@Column(name = "Cubicle_Location")
	private String cubicleLocation;

	/**
	 * @return the employeeId
	 */
	public String getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the ntId
	 */
	public String getNtId() {
		return ntId;
	}

	/**
	 * @param ntId the ntId to set
	 */
	public void setNtId(String ntId) {
		this.ntId = ntId;
	}

	

	/**
	 * @return the employeeName
	 */
	public String getEmployeeName() {
		return employeeName;
	}

	/**
	 * @param employeeName the employeeName to set
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	/**
	 * @return the departmentName
	 */
	public String getDepartmentName() {
		return departmentName;
	}

	/**
	 * @param departmentName the departmentName to set
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * @return the departmentNumber
	 */
	public String getDepartmentNumber() {
		return departmentNumber;
	}

	/**
	 * @param departmentNumber the departmentNumber to set
	 */
	public void setDepartmentNumber(String departmentNumber) {
		this.departmentNumber = departmentNumber;
	}

	/**
	 * @return the managerName
	 */
	public String getManagerName() {
		return managerName;
	}

	/**
	 * @param managerName the managerName to set
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the employmentType
	 */
	public String getEmploymentType() {
		return employmentType;
	}

	/**
	 * @param employmentType the employmentType to set
	 */
	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	/**
	 * @return the dateOfJoining
	 */
	public Date getDateOfJoining() {
		return dateOfJoining;
	}

	/**
	 * @param dateOfJoining the dateOfJoining to set
	 */
	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	/**
	 * @return the managerEmployeeId
	 */
	public String getManagerEmployeeId() {
		return managerEmployeeId;
	}

	/**
	 * @param managerEmployeeId the managerEmployeeId to set
	 */
	public void setManagerEmployeeId(String managerEmployeeId) {
		this.managerEmployeeId = managerEmployeeId;
	}

	/**
	 * @return the employmentStatus
	 */
	public String getEmploymentStatus() {
		return employmentStatus;
	}

	/**
	 * @param employmentStatus the employmentStatus to set
	 */
	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the idCreationDate
	 */
	public Date getIdCreationDate() {
		return idCreationDate;
	}

	/**
	 * @param idCreationDate the idCreationDate to set
	 */
	public void setIdCreationDate(Date idCreationDate) {
		this.idCreationDate = idCreationDate;
	}

	/**
	 * @return the extensionNumber
	 */
	public int getExtensionNumber() {
		return extensionNumber;
	}

	/**
	 * @param extensionNumber the extensionNumber to set
	 */
	public void setExtensionNumber(int extensionNumber) {
		this.extensionNumber = extensionNumber;
	}

	/**
	 * @return the cubicleLocation
	 */
	public String getCubicleLocation() {
		return cubicleLocation;
	}

	/**
	 * @param cubicleLocation the cubicleLocation to set
	 */
	public void setCubicleLocation(String cubicleLocation) {
		this.cubicleLocation = cubicleLocation;
	}

	/**
	 * @return the uId
	 */
	public int getuId() {
		return uId;
	}

	/**
	 * @param uId the uId to set
	 */
	public void setuId(int uId) {
		this.uId = uId;
	}
	
	/**
	 * toString() return all the variable values
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}
