package com.pilot.models;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AdditionalAttributes {
	
	private List<Attribute> additionalAttributes;

	public List<Attribute> getAdditionalAttributes() {
		return additionalAttributes;
	}

	public void setAdditionalAttributes(List<Attribute> additionalAttributes) {
		this.additionalAttributes = additionalAttributes;
	}

	/**
	 * toString() return all the variable values
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
