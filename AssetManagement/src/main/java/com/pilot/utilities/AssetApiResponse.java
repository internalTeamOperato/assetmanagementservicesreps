package com.pilot.utilities;

public class AssetApiResponse {

	private int ResponseErrorCode;
	private String ResponseErrorMsg;
	public int getResponseErrorCode() {
		return ResponseErrorCode;
	}
	public void setResponseErrorCode(int responseErrorCode) {
		ResponseErrorCode = responseErrorCode;
	}
	public String getResponseErrorMsg() {
		return ResponseErrorMsg;
	}
	public void setResponseErrorMsg(String responseErrorMsg) {
		ResponseErrorMsg = responseErrorMsg;
	}
	
	
}
