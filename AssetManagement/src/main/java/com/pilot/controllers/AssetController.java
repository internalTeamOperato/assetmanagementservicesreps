package com.pilot.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.pilot.models.AssetMasterData;
import com.pilot.services.AssetService;
import com.pilot.utilities.AssetApiResponse;

@CrossOrigin
@RestController
@RequestMapping(value="/Asset")
public class AssetController {
	
	@Autowired
	AssetService assetService;
	
	@RequestMapping(value = "/addAsset", method = RequestMethod.POST,  produces="application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AssetApiResponse> addAsset(@RequestBody AssetMasterData assetMasterData){
		
		
		
		assetService.addAsset(assetMasterData);
		
				
		return new ResponseEntity<AssetApiResponse>(new AssetApiResponse(), HttpStatus.OK);
		
	
		
	}
	
	
	@RequestMapping(value = "/listAsset", method = RequestMethod.GET,  produces="application/json")
	public ResponseEntity<List<AssetMasterData>> listAsset(){
		
		List<AssetMasterData> listMasterAssetData = new ArrayList<>();
		
		listMasterAssetData = assetService.listAsset();
		
				
		return new ResponseEntity<List<AssetMasterData>>(listMasterAssetData, HttpStatus.OK);
		
	
		
	}
	
	@RequestMapping(value = "/editAsset", method = RequestMethod.POST,  produces="application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AssetApiResponse> editAsset(@RequestBody AssetMasterData assetMasterData){
		
		
		
		assetService.editAsset(assetMasterData);
		
				
		return new ResponseEntity<AssetApiResponse>(new AssetApiResponse(), HttpStatus.OK);
		
	
		
	}

}
