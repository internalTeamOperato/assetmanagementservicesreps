/**
 * 
 */
package com.pilot.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pilot.models.AssetMasterData;
import com.pilot.models.Employee;
import com.pilot.services.EmployeeService;
import com.pilot.utilities.AssetApiResponse;

/**
 * @author Lowes
 *
 */
@CrossOrigin
@RestController
@RequestMapping(value="/Employee")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;
	
	@RequestMapping(value = "/addEmployee", method = RequestMethod.POST,  produces="application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AssetApiResponse> addEmployee(@RequestBody Employee employee){
		
		String returnCode = employeeService.addEmployee(employee);
				AssetApiResponse assetApiResponse = new AssetApiResponse();
				assetApiResponse.setResponseErrorMsg(returnCode);
		return new ResponseEntity<AssetApiResponse>(assetApiResponse, HttpStatus.OK);
	
	}
	
	
	@RequestMapping(value = "/searchEmployee", method = RequestMethod.POST,  produces="application/json",consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Employee>> searchEmployee(@RequestBody Employee employee){
		
		List<Employee> listEmployee = new ArrayList<>();
		
		listEmployee = employeeService.searchEmployee(employee.getuId());
		
				
		return new ResponseEntity<List<Employee>>(listEmployee, HttpStatus.OK);
		
	
		
	}
	
	@RequestMapping(value = "/searchEmployeeOnName", method = RequestMethod.POST,  produces="application/json",consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Employee>> searchEmployeeOnName(@RequestBody String employeeName){
		
		List<Employee> listEmployee = new ArrayList<>();
		
		listEmployee = employeeService.searchEmployeeBasedOnName(employeeName);
		
				
		return new ResponseEntity<List<Employee>>(listEmployee, HttpStatus.OK);
		
	
		
	}
}
