package com.pilot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pilot.models.AssetDetails;
import com.pilot.services.AssetDetailsService;
import com.pilot.utilities.AssetApiResponse;

@CrossOrigin
@RestController
@RequestMapping(value="/AssetDetail")
public class AssetDetailsController {

	//Log log = new SLF4JLogFactory().getInstance(AssetDetailsController.class);
	
	@Autowired
	AssetDetailsService assetDetailsService;
	
	@RequestMapping(value = "/addAssetDetail", method = RequestMethod.POST,  produces="application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AssetApiResponse> addAssetDetail(@RequestBody AssetDetails assetDetails){
		
		int returnCode = assetDetailsService.addAssetDetails(assetDetails);
		
		AssetApiResponse res = new AssetApiResponse();
		if(returnCode==0)
			res.setResponseErrorCode(0);
		else
			res.setResponseErrorCode(-1);
		
		return new ResponseEntity<AssetApiResponse>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateAssetDetail", method = RequestMethod.POST,  produces="application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AssetApiResponse> updateAssetDetail(@RequestBody AssetDetails assetDetails){
		
		int returnCode = assetDetailsService.updateAssetDetails(assetDetails);
		
		AssetApiResponse res = new AssetApiResponse();
		if(returnCode==0)
			res.setResponseErrorCode(0);
		else
			res.setResponseErrorCode(-1);
		
		return new ResponseEntity<AssetApiResponse>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deleteAssetDetail", method = RequestMethod.POST,  produces="application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AssetApiResponse> deleteAssetDetail(@RequestBody AssetDetails assetDetails){
		
		int returnCode = assetDetailsService.deleteAssetDetails(assetDetails);
		
		AssetApiResponse res = new AssetApiResponse();
		if(returnCode==0)
			res.setResponseErrorCode(0);
		else
			res.setResponseErrorCode(-1);
		
		return new ResponseEntity<AssetApiResponse>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAssets", method = RequestMethod.GET,  produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AssetDetails>> getAssets(){
		
		List<AssetDetails> listOfAssets = assetDetailsService.getListOfAssets();
		
		
		return new ResponseEntity<List<AssetDetails>>(listOfAssets, HttpStatus.OK);
	}
}
