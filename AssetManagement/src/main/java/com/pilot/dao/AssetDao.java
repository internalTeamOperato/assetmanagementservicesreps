package com.pilot.dao;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.pilot.models.AssetMasterData;

@Transactional
public class AssetDao {
	
	@PersistenceContext
	private EntityManager manager;
	
	/**
	 * 
	 * @param assetMasterData
	 * @return
	 */
	
	
	public int addAsset(AssetMasterData assetMasterData){
		
		try
		{
		manager.persist(assetMasterData);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return 0;
		
	}

	/**
	 * 
	 * @return
	 */
	public List<AssetMasterData> listAsset() {
		
		List<AssetMasterData> listAssetMasterData = manager.createQuery("select e from AssetMasterData e",
			    AssetMasterData.class).getResultList();
		
		return listAssetMasterData;
	}

	public int editAsset(AssetMasterData assetMasterData){
		
		try{
		manager.merge(assetMasterData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
		
	}
	
	public int deleteAsset(AssetMasterData assetMasterData){
		
		try{
			
		AssetMasterData assetMasterDataEn = manager.find(AssetMasterData.class,assetMasterData.getAssetId());
			
		manager.remove(assetMasterDataEn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
		
	}
}
