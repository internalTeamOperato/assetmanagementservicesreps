package com.pilot.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.pilot.models.AssetDetails;

@Transactional
public class AssetDetailsDao {

	@PersistenceContext
	private EntityManager manager;
	
	public int addAssetDetails(AssetDetails assetDetails) {
		try
		{
		manager.persist(assetDetails);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return -1;
		}
		return 0;
	}
	
	public int updateAssetDetails(AssetDetails assetDetails) {
		try
		{
		manager.merge(assetDetails);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return -1;
		}
		return 0;
	}
	
	public List<AssetDetails> getListOfAssets(){
		List<AssetDetails> resList = manager.createQuery("select e from AssetDetails e",
			    AssetDetails.class).getResultList();
		return resList;
	}
	
	public int deleteAssetDetail(AssetDetails assetDetails) {
		try{
			//check if we need the below line
			AssetDetails assetDetails1 = manager.find(AssetDetails.class,assetDetails.getAssetId());
				
			manager.remove(assetDetails1);
			} catch (Exception e) {
				e.printStackTrace();
				return -1;
			}
			return 0;
	}
}
