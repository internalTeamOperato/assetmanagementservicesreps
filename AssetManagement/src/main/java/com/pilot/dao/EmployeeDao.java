/**
 * 
 */
package com.pilot.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.pilot.models.Employee;

/**
 * @author Lowes
 *
 */

public class EmployeeDao {

	
	@PersistenceContext
	private EntityManager manager;
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int addEmployee(Employee employee) {
		int returnCode = 0;
		try {
		if(null == manager.find(Employee.class, employee.getuId())) {	
		manager.persist(employee);
		}else {
			returnCode = 8;
		}
		}catch( javax.persistence.PersistenceException  ex) {
		 
	    	ex.printStackTrace();
	      
		}finally {
			manager.close();
		}

		return returnCode;
	}
	
	
	/**
	 * Search employee based on multiple criteria
	 */
	public List<Employee> searchEmployee(int salesId) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();

	    CriteriaQuery<Employee> query = builder.createQuery(Employee.class);
	    Root<Employee> from = query.from(Employee.class);
	    CriteriaQuery<Employee> select = query.select(from);
	    
	    Predicate p1 = builder.equal(from.get("uId"), salesId);
	    query.where(p1);
		return manager.createQuery(select).getResultList();
		
	}

	/**
	 * 
	 * @param employeeName
	 * @return
	 */
	public List<Employee> searchEmployeeBasedOnName(String employeeName) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();

	    CriteriaQuery<Employee> query = builder.createQuery(Employee.class);
	    Root<Employee> from = query.from(Employee.class);
	    CriteriaQuery<Employee> select = query.select(from);
	    
	    Predicate p1 = builder.like(from.get("employeeName"), employeeName);
	    query.where(p1);
		return manager.createQuery(select).getResultList();
	}
	
}
