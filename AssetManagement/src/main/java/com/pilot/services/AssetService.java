package com.pilot.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pilot.dao.AssetDao;
import com.pilot.models.AssetMasterData;


@Service
public class AssetService {

	@Autowired
	AssetDao assetDao;
	
	public String addAsset(AssetMasterData assetMasterData){
		
		//set update date
		assetMasterData.setUpdateDate(new Date());
		
		//set temp asset id
		assetMasterData.setUpdateId("A.ID");
		
		int returnCode = assetDao.addAsset(assetMasterData);
		if (returnCode == 0){
			return "Success";
		}
		else{
			return "Failure";	
		}
			
	}

	public List<AssetMasterData> listAsset() {
	
		List<AssetMasterData> listAssetMasterData = assetDao.listAsset();
		
		return listAssetMasterData;
	}
	

	public String editAsset(AssetMasterData assetMasterData){
		
		int returnCode = assetDao.editAsset(assetMasterData);
		if (returnCode == 0){
			return "Success";
		}
		else{
			return "Failure";	
		}
			
	}

	

}
