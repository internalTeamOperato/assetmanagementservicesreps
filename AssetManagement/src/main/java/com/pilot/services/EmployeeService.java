package com.pilot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.pilot.dao.EmployeeDao;
import com.pilot.models.Employee;

public class EmployeeService {

@Autowired
EmployeeDao employeeDao;
		
/**
 * 	
 * @param employee
 * @return
 */
public String addEmployee(Employee employee) {
	
	int returnCode = employeeDao.addEmployee(employee);
	if (returnCode == 0){
		return "Success";
	}
	else{
		return "Duplicate UID entered";	
	}
	}

/**
 * 
 */
public List<Employee> searchEmployee(int salesId){
	
	return employeeDao.searchEmployee(salesId);
	
}

/**
 * 
 * @param employeeName
 * @return
 */
public List<Employee> searchEmployeeBasedOnName(String employeeName) {
	
	List<Employee> listEmployee =  employeeDao.searchEmployeeBasedOnName(employeeName);
	return listEmployee;
	
}
	
}
