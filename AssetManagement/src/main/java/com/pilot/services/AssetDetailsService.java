package com.pilot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pilot.dao.AssetDetailsDao;
import com.pilot.models.AssetDetails;

@Service
public class AssetDetailsService {

	@Autowired
	AssetDetailsDao assetDetailsDao;
	
	public int addAssetDetails(AssetDetails assetDetails)
	{
		return assetDetailsDao.addAssetDetails(assetDetails);
	}
	
	public List<AssetDetails> getListOfAssets(){
		return assetDetailsDao.getListOfAssets();
	}
	
	public int updateAssetDetails(AssetDetails assetDetails) {
		return assetDetailsDao.updateAssetDetails(assetDetails);
	}
	
	public int deleteAssetDetails(AssetDetails assetDetails) {
		return assetDetailsDao.deleteAssetDetail(assetDetails);
	}
}
